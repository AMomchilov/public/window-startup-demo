//
//  ViewController.swift
//  WindowStartupDemo
//
//  Created by Alexander Momchilov on 2020-12-25.
//

import Cocoa

class ViewController: NSViewController {
	
	var fooDependency: FooDependency!
	
	func initializeDependencies(fooDependency: FooDependency) {
		self.fooDependency = fooDependency
	}
	
	override func viewWillAppear() {
		print("ViewController.\(#function)")
		super.viewDidLoad()

		print(fooDependency.randomNumber()) // 🧨 this isn't set in time
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}
}

