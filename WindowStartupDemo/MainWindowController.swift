//
//  MainWindowController.swift
//  WindowStartupDemo
//
//  Created by Alexander Momchilov on 2020-12-25.
//

import AppKit

class MainWindowController: NSWindowController {
	var fooDependency: FooDependency!
	
	override func windowDidLoad() {
		print("MainWindowController.\(#function)")
		self.contentVC.fooDependency = fooDependency
	}
	
	var contentVC: ViewController { self.contentViewController as! ViewController }
	
	func initializeDependencies(fooDependency: FooDependency) {
		print("MainWindowController.\(#function)")
		self.fooDependency = fooDependency
	}
}
