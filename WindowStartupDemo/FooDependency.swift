//
//  FooDependency.swift
//  WindowStartupDemo
//
//  Created by Alexander Momchilov on 2020-12-25.
//

import Foundation

class FooDependency {
	func randomNumber() -> Int {
		Int.random(in: 0...100)
	}
}
