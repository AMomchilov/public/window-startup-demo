//
//  AppDelegate.swift
//  WindowStartupDemo
//
//  Created by Alexander Momchilov on 2020-12-25.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

	// Ideally this is created once app-wide, and shared among all the windows that need it.
	let fooDependency = FooDependency()
	
	lazy var mainWindowController: MainWindowController = {
		
		let storyboard = NSStoryboard(name: "MainWindow", bundle: nil)
		
        // This API was introduced in 10.15, probably for my exact problem.
		// https://developer.apple.com/documentation/appkit/nsstoryboard/3203780-instantiateinitialcontroller
		let vc = storyboard.instantiateInitialController(
			creator: { [fooDependency] coder -> MainWindowController in
				let vc = MainWindowController(coder: coder)!
				vc.initializeDependencies(fooDependency: fooDependency)
				return vc
			}
		)
		
		return vc!
	}()

	func applicationDidFinishLaunching(_ aNotification: Notification) {
		mainWindowController.showWindow(self)
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application
	}


}

